/*
 * Copyright (c) 2019, CATIE, All Rights Reserved
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include"mbed.h"
#include "Zest_gps/Zest_GPS.h"
#include<time.h>
//namespace sixtron {

//RawSerial uart(UART1_TX,UART1_RX,9600);

static bool sync_acq = false;

char *p= new char;
char *p1= new char;
char *p2= new char;
char *p3= new char;
char *p4= new char;
char *p5= new char;
char *p6= new char;
char *p7= new char;
char *p8= new char;

ZestGPS::ZestGPS(RawSerial *serial):
				_cb(NULL)

{
	_serial = serial;
	clear_data();
	_serial->attach(callback(this, &ZestGPS::_rx_data_handler), Serial::RxIrq);
	start=clock();
}

void ZestGPS::initialisation()
{
	/*
	 * send message to configure UART : baudrate, configuration of frame, ...
	 */
	u_int8_t UBX_CFG_PRT[28];
	u_int8_t CK_A=0;
	u_int8_t CK_B=0;
	UBX_CFG_PRT[0]=0xB5; UBX_CFG_PRT[1]=0x62; UBX_CFG_PRT[2]=0x06; UBX_CFG_PRT[3]=0x00; UBX_CFG_PRT[4]=20; UBX_CFG_PRT[5]=0; UBX_CFG_PRT[6]=0x01; UBX_CFG_PRT[7]=0x00; UBX_CFG_PRT[8]=0x00;
	UBX_CFG_PRT[9]=0x00; UBX_CFG_PRT[10]=0xC0; UBX_CFG_PRT[11]=0x08; UBX_CFG_PRT[12]=0x00; UBX_CFG_PRT[13]=0x00; UBX_CFG_PRT[14]=0x80; UBX_CFG_PRT[15]=0x25; UBX_CFG_PRT[16]=0x00;
	UBX_CFG_PRT[17]=0x00; UBX_CFG_PRT[18]=0x03; UBX_CFG_PRT[19]=0x00; UBX_CFG_PRT[20]=0x03; UBX_CFG_PRT[21]=0x00; UBX_CFG_PRT[22]=0x00; UBX_CFG_PRT[23]=0x00; UBX_CFG_PRT[24]=0x00;
	UBX_CFG_PRT[25]=0x00;
	int i;
	for (i=2;i<=25;i++)
	{
		CK_A+=UBX_CFG_PRT[i];
		CK_B+=CK_A;
	}
	UBX_CFG_PRT[26]=CK_A; UBX_CFG_PRT[27]=CK_B;
	sendTo(UBX_CFG_PRT,28);
	wait(0.1);
	/*
	 * send message to disable  NMEA frames with invalid fixes or invalid dates
	 */
	u_int8_t UBX_CFG_NMEA[28];
	CK_A=0;
	CK_B=0;
	UBX_CFG_NMEA[0]=0xB5; UBX_CFG_NMEA[1]=0x62; UBX_CFG_NMEA[2]=0x06; UBX_CFG_NMEA[3]=0x17; UBX_CFG_NMEA[4]=20; UBX_CFG_NMEA[5]=0; UBX_CFG_NMEA[6]=0x2F; UBX_CFG_NMEA[7]=0x41; UBX_CFG_NMEA[8]=0x00;
	UBX_CFG_NMEA[9]=0x01; UBX_CFG_NMEA[10]=0x00; UBX_CFG_NMEA[11]=0x00; UBX_CFG_NMEA[12]=0x00; UBX_CFG_NMEA[13]=0x40; UBX_CFG_NMEA[14]=0x01; UBX_CFG_NMEA[15]=0x03; UBX_CFG_NMEA[16]=0x01; UBX_CFG_NMEA[17]=0x01;
	UBX_CFG_NMEA[18]=0x00; UBX_CFG_NMEA[19]=0x00; UBX_CFG_NMEA[20]=0x00; UBX_CFG_NMEA[21]=0x00; UBX_CFG_NMEA[22]=0x00; UBX_CFG_NMEA[23]=0x00; UBX_CFG_NMEA[24]=0x00; UBX_CFG_NMEA[25]=0x00;
	for (i=2;i<=25;i++)
	{
		CK_A+=UBX_CFG_NMEA[i];
		CK_B+=CK_A;
	}
	UBX_CFG_NMEA[26]=CK_A; UBX_CFG_NMEA[27]=CK_B;
	sendTo(UBX_CFG_NMEA,28);
	wait(0.1);
	/*
	 * send message to disable DTM NMEA frames
	 */
	u_int8_t UBX_CFG_MSG[11];
	CK_A=0;
	CK_B=0;
	UBX_CFG_MSG[0]=0xB5; UBX_CFG_MSG[1]=0x62; UBX_CFG_MSG[2]=0x06; UBX_CFG_MSG[3]=0x01; UBX_CFG_MSG[4]=3; UBX_CFG_MSG[5]=0; UBX_CFG_MSG[6]=0xF0; UBX_CFG_MSG[7]=0x0A; UBX_CFG_MSG[8]=0x00;
	for (i=2;i<=8;i++)
	{
		CK_A+=UBX_CFG_MSG[i];
		CK_B+=CK_A;
	}
	UBX_CFG_MSG[9]=CK_A; UBX_CFG_MSG[10]=CK_B;
	sendTo(UBX_CFG_MSG,11);
	wait(0.1);
	/*
	* send message to disable GBS NMEA frames
	*/
	CK_A=0;
	CK_B=0;
	UBX_CFG_MSG[0]=0xB5; UBX_CFG_MSG[1]=0x62; UBX_CFG_MSG[2]=0x06; UBX_CFG_MSG[3]=0x01; UBX_CFG_MSG[4]=3; UBX_CFG_MSG[5]=0; UBX_CFG_MSG[6]=0xF0; UBX_CFG_MSG[7]=0x09; UBX_CFG_MSG[8]=0x00;
	for (i=2;i<=8;i++)
	{
		CK_A+=UBX_CFG_MSG[i];
		CK_B+=CK_A;
	}
	UBX_CFG_MSG[9]=CK_A; UBX_CFG_MSG[10]=CK_B;
	sendTo(UBX_CFG_MSG,11);
	wait(0.1);
	/*
	* send message to disable GLL NMEA frames
	*/
	CK_A=0;
	CK_B=0;
	UBX_CFG_MSG[0]=0xB5; UBX_CFG_MSG[1]=0x62; UBX_CFG_MSG[2]=0x06; UBX_CFG_MSG[3]=0x01; UBX_CFG_MSG[4]=3; UBX_CFG_MSG[5]=0; UBX_CFG_MSG[6]=0xF0; UBX_CFG_MSG[7]=0x01; UBX_CFG_MSG[8]=0x00;
	for (i=2;i<=8;i++)
	{
		CK_A+=UBX_CFG_MSG[i];
		CK_B+=CK_A;
	}
	UBX_CFG_MSG[9]=CK_A; UBX_CFG_MSG[10]=CK_B;
	sendTo(UBX_CFG_MSG,11);
	wait(0.1);
	/*
	* send message to disable GNS NMEA frames
	*/
	CK_A=0;
	CK_B=0;
	UBX_CFG_MSG[0]=0xB5; UBX_CFG_MSG[1]=0x62; UBX_CFG_MSG[2]=0x06; UBX_CFG_MSG[3]=0x01; UBX_CFG_MSG[4]=3; UBX_CFG_MSG[5]=0; UBX_CFG_MSG[6]=0xF0; UBX_CFG_MSG[7]=0x0D; UBX_CFG_MSG[8]=0x00;
	for (i=2;i<=8;i++)
	{
		CK_A+=UBX_CFG_MSG[i];
		CK_B+=CK_A;
	}
	UBX_CFG_MSG[9]=CK_A; UBX_CFG_MSG[10]=CK_B;
	sendTo(UBX_CFG_MSG,11);
	wait(0.1);
	/*
	* send message to disable GRS NMEA frames
	*/
	CK_A=0;
	CK_B=0;
	UBX_CFG_MSG[0]=0xB5; UBX_CFG_MSG[1]=0x62; UBX_CFG_MSG[2]=0x06; UBX_CFG_MSG[3]=0x01; UBX_CFG_MSG[4]=3; UBX_CFG_MSG[5]=0; UBX_CFG_MSG[6]=0xF0; UBX_CFG_MSG[7]=0x06; UBX_CFG_MSG[8]=0x00;
	for (i=2;i<=8;i++)
	{
		CK_A+=UBX_CFG_MSG[i];
		CK_B+=CK_A;
	}
	UBX_CFG_MSG[9]=CK_A; UBX_CFG_MSG[10]=CK_B;
	sendTo(UBX_CFG_MSG,11);
	wait(0.1);
	/*
	* send message to disable GST NMEA frames
	*/
	CK_A=0;
	CK_B=0;
	UBX_CFG_MSG[0]=0xB5; UBX_CFG_MSG[1]=0x62; UBX_CFG_MSG[2]=0x06; UBX_CFG_MSG[3]=0x01; UBX_CFG_MSG[4]=3; UBX_CFG_MSG[5]=0; UBX_CFG_MSG[6]=0xF0; UBX_CFG_MSG[7]=0x07; UBX_CFG_MSG[8]=0x00;
	for (i=2;i<=8;i++)
	{
		CK_A+=UBX_CFG_MSG[i];
		CK_B+=CK_A;
	}
	UBX_CFG_MSG[9]=CK_A; UBX_CFG_MSG[10]=CK_B;
	sendTo(UBX_CFG_MSG,11);
	wait(0.1);
	/*
	* send message to disable RMC NMEA frames
	*/
	CK_A=0;
	CK_B=0;
	UBX_CFG_MSG[0]=0xB5; UBX_CFG_MSG[1]=0x62; UBX_CFG_MSG[2]=0x06; UBX_CFG_MSG[3]=0x01; UBX_CFG_MSG[4]=3; UBX_CFG_MSG[5]=0; UBX_CFG_MSG[6]=0xF0; UBX_CFG_MSG[7]=0x04; UBX_CFG_MSG[8]=0x00;
	for (i=2;i<=8;i++)
	{
		CK_A+=UBX_CFG_MSG[i];
		CK_B+=CK_A;
	}
	UBX_CFG_MSG[9]=CK_A; UBX_CFG_MSG[10]=CK_B;
	sendTo(UBX_CFG_MSG,11);
	wait(0.1);
	/*
	* send message to disable VLW NMEA frames
	*/
	CK_A=0;
	CK_B=0;
	UBX_CFG_MSG[0]=0xB5; UBX_CFG_MSG[1]=0x62; UBX_CFG_MSG[2]=0x06; UBX_CFG_MSG[3]=0x01; UBX_CFG_MSG[4]=3; UBX_CFG_MSG[5]=0; UBX_CFG_MSG[6]=0xF0; UBX_CFG_MSG[7]=0x0F; UBX_CFG_MSG[8]=0x00;
	for (i=2;i<=8;i++)
	{
		CK_A+=UBX_CFG_MSG[i];
		CK_B+=CK_A;
	}
	UBX_CFG_MSG[9]=CK_A; UBX_CFG_MSG[10]=CK_B;
	sendTo(UBX_CFG_MSG,11);
	wait(0.1);
	/*
	* send message to disable GSA NMEA frames
	*/
	CK_A=0;
	CK_B=0;
	UBX_CFG_MSG[0]=0xB5; UBX_CFG_MSG[1]=0x62; UBX_CFG_MSG[2]=0x06; UBX_CFG_MSG[3]=0x01; UBX_CFG_MSG[4]=3; UBX_CFG_MSG[5]=0; UBX_CFG_MSG[6]=0xF0; UBX_CFG_MSG[7]=0x02; UBX_CFG_MSG[8]=0x00;
	for (i=2;i<=8;i++)
	{
		CK_A+=UBX_CFG_MSG[i];
		CK_B+=CK_A;
	}
	UBX_CFG_MSG[9]=CK_A; UBX_CFG_MSG[10]=CK_B;
	sendTo(UBX_CFG_MSG,11);
	wait(0.1);
	/*
	* send message to disable ZDA NMEA frames
	*/
	CK_A=0;
	CK_B=0;
	UBX_CFG_MSG[0]=0xB5; UBX_CFG_MSG[1]=0x62; UBX_CFG_MSG[2]=0x06; UBX_CFG_MSG[3]=0x01; UBX_CFG_MSG[4]=3; UBX_CFG_MSG[5]=0; UBX_CFG_MSG[6]=0xF0; UBX_CFG_MSG[7]=0x08; UBX_CFG_MSG[8]=0x00;
	for (i=2;i<=8;i++)
	{
		CK_A+=UBX_CFG_MSG[i];
		CK_B+=CK_A;
	}
	UBX_CFG_MSG[9]=CK_A; UBX_CFG_MSG[10]=CK_B;
	sendTo(UBX_CFG_MSG,11);
	wait(0.2);
	/*
	 * send message to enable the following satellites : GPS, GLONASS and GALILEO
	 */
	u_int8_t UBX_CFG_GNSS[20];
	CK_A=0;
	CK_B=0;
	UBX_CFG_GNSS[0]=0xB5; UBX_CFG_GNSS[1]=0x62; UBX_CFG_GNSS[2]=0x06; UBX_CFG_GNSS[3]=0x3E; UBX_CFG_GNSS[4]=12; UBX_CFG_GNSS[5]=0x00; UBX_CFG_GNSS[6]=0x00; UBX_CFG_GNSS[7]=0x00; UBX_CFG_GNSS[8]=0x20;
	UBX_CFG_GNSS[9]=0x01; UBX_CFG_GNSS[10]=0x02; UBX_CFG_GNSS[11]=0x04; UBX_CFG_GNSS[12]=0x08; UBX_CFG_GNSS[13]=0x00; UBX_CFG_GNSS[14]=0x01; UBX_CFG_GNSS[15]=0x00; UBX_CFG_GNSS[16]=0x01;
	UBX_CFG_GNSS[17]=0x01;
	for (i=2;i<=17;i++)
	{
		CK_A+=UBX_CFG_GNSS[i];
		CK_B+=CK_A;
	}
	UBX_CFG_GNSS[18]=CK_A;
	UBX_CFG_GNSS[19]=CK_B;
	sendTo(UBX_CFG_GNSS,20);
	wait(0.5);
	/*
	 * send message to save configuration as permanent configuration
	 */
	u_int8_t UBX_CFG_CFG[20];
	CK_A=0;
	CK_B=0;
	UBX_CFG_CFG[0]=0xB5; UBX_CFG_CFG[1]=0x62; UBX_CFG_CFG[2]=0x06; UBX_CFG_CFG[3]=0x09; UBX_CFG_CFG[4]=12; UBX_CFG_CFG[5]=0x00; UBX_CFG_CFG[6]=0x00; UBX_CFG_CFG[7]=0x00; UBX_CFG_CFG[8]=0x00;
	UBX_CFG_CFG[9]=0x00; UBX_CFG_CFG[10]=0x02; UBX_CFG_CFG[11]=0x00; UBX_CFG_CFG[12]=0x00; UBX_CFG_CFG[13]=0x00; UBX_CFG_CFG[14]=0x00; UBX_CFG_CFG[15]=0x00; UBX_CFG_CFG[16]=0x00;
	UBX_CFG_CFG[17]=0x00; UBX_CFG_CFG[18]=0x01;
	for (i=2;i<=18;i++)
	{
		CK_A+=UBX_CFG_CFG[i];
		CK_B+=CK_A;
	}
	UBX_CFG_CFG[19]=CK_A;
	UBX_CFG_CFG[20]=CK_B;
	sendTo(UBX_CFG_CFG,21);
	wait(0.1);
}




void ZestGPS::sendTo(u_int8_t c[],int l)
{
	_serial->write(c,l,NULL);
}

void ZestGPS::setAGNSSAutonomous()
{
	u_int8_t UBX_CFG_NAVX5[48];
	u_int8_t CK_A=0;
	u_int8_t CK_B=0;
    UBX_CFG_NAVX5[0]=0xB5; UBX_CFG_NAVX5[1]=0x62; UBX_CFG_NAVX5[2]=0x06; UBX_CFG_NAVX5[3]=0x23; UBX_CFG_NAVX5[4]=40; UBX_CFG_NAVX5[5]=0; UBX_CFG_NAVX5[6]=0x00; UBX_CFG_NAVX5[7]=0x00; UBX_CFG_NAVX5[8]=0x00;
    UBX_CFG_NAVX5[9]=0x04; UBX_CFG_NAVX5[10]=0x00; UBX_CFG_NAVX5[11]=0x00; UBX_CFG_NAVX5[12]=0x00; UBX_CFG_NAVX5[13]=0x00; UBX_CFG_NAVX5[14]=0x00; UBX_CFG_NAVX5[15]=0x00; UBX_CFG_NAVX5[16]=0x00;
    UBX_CFG_NAVX5[17]=0x00; UBX_CFG_NAVX5[18]=0x00; UBX_CFG_NAVX5[19]=0x00; UBX_CFG_NAVX5[20]=0x00; UBX_CFG_NAVX5[21]=0x00; UBX_CFG_NAVX5[22]=0x00; UBX_CFG_NAVX5[23]=0x01; UBX_CFG_NAVX5[24]=0x00;
    UBX_CFG_NAVX5[25]=0x00; UBX_CFG_NAVX5[26]=0x00; UBX_CFG_NAVX5[27]=0x00; UBX_CFG_NAVX5[28]=0x00; UBX_CFG_NAVX5[29]=0x00; UBX_CFG_NAVX5[30]=0x00; UBX_CFG_NAVX5[31]=0x00; UBX_CFG_NAVX5[32]=0x00;
    UBX_CFG_NAVX5[33]=0x01; UBX_CFG_NAVX5[34]=0x00; UBX_CFG_NAVX5[35]=0x00; UBX_CFG_NAVX5[36]=0x00; UBX_CFG_NAVX5[37]=0x00; UBX_CFG_NAVX5[38]=0x00; UBX_CFG_NAVX5[39]=0x00; UBX_CFG_NAVX5[40]=0x00;
    UBX_CFG_NAVX5[41]=0x00; UBX_CFG_NAVX5[42]=0x00; UBX_CFG_NAVX5[43]=0x00; UBX_CFG_NAVX5[44]=0x00; UBX_CFG_NAVX5[45]=0x00;
	int i;
	for (i=2;i<=45;i++)
	{
		CK_A+=UBX_CFG_NAVX5[i];
		CK_B+=CK_A;
	}
	UBX_CFG_NAVX5[46]=CK_A; UBX_CFG_NAVX5[47]=CK_B;
	sendTo(UBX_CFG_NAVX5,48);
	wait(0.1);
	start=clock();
}

void ZestGPS::setLowPower()
{
	u_int8_t UBX_CFG_PMS[16];
	u_int8_t CK_A=0;
	u_int8_t CK_B=0;
	UBX_CFG_PMS[0]=0xB5; UBX_CFG_PMS[1]=0x62; UBX_CFG_PMS[2]=0x06; UBX_CFG_PMS[3]=0x86; UBX_CFG_PMS[4]=8; UBX_CFG_PMS[5]=0; UBX_CFG_PMS[6]=0x00; UBX_CFG_PMS[7]=0x01; UBX_CFG_PMS[8]=0x00;
	UBX_CFG_PMS[9]=0x00; UBX_CFG_PMS[10]=0x00; UBX_CFG_PMS[11]=0x00; UBX_CFG_PMS[12]=0x00; UBX_CFG_PMS[13]=0x00;
    int i;
    for (i=2;i<=13;i++)
	{
		CK_A+=UBX_CFG_PMS[i];
		CK_B+=CK_A;
	}
	UBX_CFG_PMS[14]=CK_A; UBX_CFG_PMS[15]=CK_B;
	sendTo(UBX_CFG_PMS,16);
	wait(0.1);
}





double ZestGPS::latitude(char nmea[])
{
	double latitude;
	double d=0.0;
	d=(int((char*)nmea[18])-48)*10;
	d=d+(int((char*)nmea[19])-48);
	double m=0.0;
	int i;
	m=(int((char*)nmea[20])-48)*10;
	m=m+(int((char*)nmea[21])-48);
	for(i=1;i<5;i++)
	{
		m=m+(int((char*)nmea[22+i])-48)*pow(0.1,i);
	}
	latitude=d+(m/60);
	return latitude;
}


double ZestGPS::longitude(char nmea[])
{
	double latitude;
	double d;
	d=(int((char*)nmea[30])-48)*100;
	d=d+(int((char*)nmea[31])-48)*10;
	d=d+(int((char*)nmea[32])-48);
	double m;
	int i;
	m=(int((char*)nmea[33])-48)*10;
	m=m+(int((char*)nmea[34])-48);
	for(i=1;i<5;i++)
	{
		m=m+(int((char*)nmea[35+i])-48)*pow(0.1,i);
	}
	latitude=d+(m/60);
	return latitude;
}




double ZestGPS::TTFF(char nmea[])
{
	double ttff;
	clock_t end=clock();
	ttff=((double)end-start)/CLOCKS_PER_SEC;
	return ttff;
}


bool ZestGPS::first_fix(char nmea[])
{
	bool flag=false;
	char f='G';
	char s='G';
	char t='A';
	char e='1';
	char g='2';
	char f1=nmea[3];
	char s1=nmea[4];
	char t1=nmea[5];
	char e1=nmea[44];
	p=&f1;
	p1=&s1;
	p2=&t1;
	p3=&e1;
	p4=&f;
	p5=&s;
	p6=&t;
	p7=&e;
	p8=&g;
	if (((memcmp(p,p4,1))==0) and ((memcmp(p1,p5,1))==0) and ((memcmp(p2,p6,1))==0) and (((memcmp(p3,p7,1))==0)or((memcmp(p3,p8,1))==0)))
	{
		flag=true;
	}
	return flag;
}

bool ZestGPS::parseGSV(char nmea[])
{
	bool gsv=false;
	char f='G';
	char s='S';
	char t='V';
	char f1=nmea[3];
	char s1=nmea[4];
	char t1=nmea[5];
	p=&f1;
	p1=&s1;
	p2=&t1;
	p3=&f;
	p4=&s;
	p5=&t;
	if (((memcmp(p,p3,1))==0) and ((memcmp(p1,p4,1))==0) and ((memcmp(p2,p5,1))==0))
	{
		gsv=true;
	}
	return gsv;
}

bool ZestGPS::parseVTG(char nmea[])
{
	bool vtg=false;
	char f='V';
	char s='T';
	char t='G';
	char f1=nmea[3];
	char s1=nmea[4];
	char t1=nmea[5];
	p=&f1;
	p1=&s1;
	p2=&t1;
	p3=&f;
	p4=&s;
	p5=&t;
	if (((memcmp(p,p3,1))==0) and ((memcmp(p1,p4,1))==0) and ((memcmp(p2,p5,1))==0))
	{
		vtg=true;
	}
	return vtg;
}

bool ZestGPS::parseGGA(char nmea[])
{
	bool gga=false;
	char f='G';
	char s='G';
	char t='A';
	char f1=nmea[3];
	char s1=nmea[4];
	char t1=nmea[5];
	p=&f1;
	p1=&s1;
	p2=&t1;
	p3=&f;
	p4=&s;
	p5=&t;
	if (((memcmp(p,p3,1))==0) and ((memcmp(p1,p4,1))==0) and ((memcmp(p2,p5,1))==0))
	{
		gga=true;
	}
	return gga;
}

bool ZestGPS::parseGSA(char nmea[])
{
	bool gsa=false;
	char f='G';
	char s='S';
	char t='A';
	char f1=nmea[3];
	char s1=nmea[4];
	char t1=nmea[5];
	p=&f1;
	p1=&s1;
	p2=&t1;
	p3=&f;
	p4=&s;
	p5=&t;
	if (((memcmp(p,p3,1))==0) and ((memcmp(p1,p4,1))==0) and ((memcmp(p2,p5,1))==0))
	{
		gsa=true;
	}
	return gsa;
}


void ZestGPS::clear_data(void)
{
	memset(_ublox_data.buff, '\0', UBLOX_BUFFER_SIZE);
	_ublox_data.ready = false;
	_ublox_data.offset = 0;
	sync_acq = false;
	_ublox_data.nbr_trame = 0;
}

void ZestGPS::reset_data(void)
{
	_ublox_data.ready =false;
	_ublox_data.offset = 0;
}

char* ZestGPS::data(void)
{
	return _ublox_data.buff;
}

size_t ZestGPS::read_data(char *dest)
{
	size_t length = 0;
	char *ptr = _ublox_data.buff;

	if (_ublox_data.ready == true) {
		for (length  = 0; *ptr != '\0' || (length >= (UBLOX_BUFFER_SIZE - 1)) ; length++, ptr++, dest++) {
			*dest = *ptr;
		}
		if (length >= (UBLOX_BUFFER_SIZE - 1)) {
			//error
			return -1;
		}
		return length;
	}
	return 0;
}

bool ZestGPS::data_available(void)
{
	return _ublox_data.ready;
}

void ZestGPS::attach(Callback<void()> func)
{
	_cb = func;
}

void ZestGPS::_rx_data_handler(void)
{
	char c;

	c = _serial->getc();

	//echo
	//_serial->putc(c);

	// debut d'une trame NMEA
	if (c == '$') {
		sync_acq = true;
	}

	if (sync_acq == true) {
		_ublox_data.buff[_ublox_data.offset] = c;
		 if (c == '\n') {
			 _ublox_data.nbr_trame++;
		 }

		 if (_ublox_data.nbr_trame >= UBLOX_MAX_TRAME) {
			 sync_acq = false;
			 _ublox_data.nbr_trame = 0;
			 _ublox_data.offset++;
			 _ublox_data.buff[_ublox_data.offset] = '\0';
			 _ublox_data.offset = 0;
			 _ublox_data.ready = true;
			 if (this->_cb) {
				 this->_cb.call();
			 }
		 } else {
			 _ublox_data.offset++;
		 }
	}

	// gestion overflow
	if (_ublox_data.offset >=(UBLOX_BUFFER_SIZE - 1)) {
		_ublox_data.offset = 0;
	}
}
//} // namespace sixtron
