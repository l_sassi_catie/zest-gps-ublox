/*
 * Copyright (c) 2019, CATIE, All Rights Reserved
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef CATIE_SIXTRON_ZEST_GPS_H_
#define CATIE_SIXTRON_ZEST_GPS_H_

#include"mbed.h"

//namespace sixtron {

#define UBLOX_MAX_TRAME 1
#define UBLOX_BUFFER_SIZE (100 * UBLOX_MAX_TRAME)

typedef struct {
	char buff[UBLOX_BUFFER_SIZE];
	uint16_t offset;
	bool ready;
	uint8_t nbr_trame;
}ublox_data_t;


class ZestGPS
{


public:
	/**
	 * class ZestGPS
	 */
    ZestGPS(RawSerial *serial);


    void initialisation();
    void sendTo(u_int8_t c[],int l);
    void setLowPower();
    void setFullPower();
    void setDGNSS();
    void setAGNSSAutonomous();
    void PowerOn();
    void PowerOff();
    double latitude(char nmea[]);
    double longitude(char nmea[]);
    double TTFF(char nmea[]);
    bool first_fix(char nmea[]);
    void attach(Callback<void()> func);
    bool data_available(void);
    char* data(void);
    size_t read_data(char *dest);
    void clear_data(void);
    void reset_data(void);
    bool parseGSV(char nmea[]);
    bool parseVTG(char nmea[]);
    bool parseGGA(char nmea[]);
    bool parseGSA(char nmea[]);
protected:
    void _rx_data_handler(void);

private:
    RawSerial *_serial;
    ublox_data_t _ublox_data;
    Callback<void()> _cb;
    clock_t start;

};

//} // namespace sixtron

#endif // CATIE_SIXTRON_ZEST_GPS_H_
