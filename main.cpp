/*
 * Copyright (c) 2019, CATIE, All Rights Reserved
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "mbed.h"
#include "Zest_GPS/Zest_GPS.h"
#include<time.h>

// prototypes
void clear_data(void);

// Peripherals
static DigitalOut led1(LED1);
RawSerial ser(UART1_TX, UART1_RX, 9600);
RawSerial pc(SERIAL_TX, SERIAL_RX, 9600);
InterruptIn button(BUTTON1);
ZestGPS gps(&ser);


// RTOS
Thread thread_led;

// global variables
char data[300];
bool data_ready = false;
bool fix=false;

static void user_ublox_callback(void)
{
	gps.read_data(data);
	data_ready = true;
}

void clear_data(void)
{
	memset(data, '\0', sizeof(data));
	data_ready = false;
}

void process_led(void)
{
	while (true) {
		ThisThread::sleep_for(250);
		led1 = !led1;
	}
}

void fix_parse(char nmea[])
{
	if(!fix)
	{
		if (gps.first_fix(nmea))
		{

			fix=true;
			double ttff=gps.TTFF(nmea);
			double l=gps.latitude((char*)nmea);
			double L=gps.longitude((char*)nmea);
			printf("TTFF: %f \r\n",ttff);
			printf("latidude : %f \r\n",l);
			printf("longitude : %f \r\n",L);


		}
	}
	else
	{
		if(gps.parseGGA(nmea))
		{
			double l=gps.latitude((char*)nmea);
			double L=gps.longitude((char*)nmea);
			printf("latidude : %f \r\n",l);
			printf("longitude : %f \r\n",L);
		}
	}

}

// main() runs in its own thread in the OS
// (note the calls to Thread::wait below for delays)
int main()
{
	char nmea[100]="$GPGGA,092725.00,4717.11399,N,00833.91590,E,1,08,1.01,499.6,M,48.0,M,,*5B\r\n";
	fix_parse(nmea);
//	fix=false;
//	gps.initialisation();


	clear_data();

	gps.attach(user_ublox_callback);



//	pc.printf("\r\nApplication started\r\n");

	while (true) {
		if (data_ready == true) {
			thread_led.start(process_led);
			fix_parse(data);
			pc.printf("%s", data);
			clear_data();
		} else {
			ThisThread::sleep_for(1);
		}
	}

}












